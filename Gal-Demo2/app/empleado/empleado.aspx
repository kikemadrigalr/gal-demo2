﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="empleado.aspx.cs" Inherits="app_empleado_empleado" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous"/>
    <link href="../css/styles.css" rel="stylesheet" type="text/css"/>
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>

    <title>GAL-DEMO - Empleados</title>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                    <a class="navbar-brand" href="../Default.aspx">GAL-DEMO</a>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="../Default.aspx">Inicio <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="empleado.aspx">Empleados</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#">Clientes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#">Proyectos</a>
                        </li>
                    </ul>
                </div>
            </nav>
    
    <form id="form1" runat="server">

        <div class="container">

              <h2 class="titulo2">Empleados</h2>

            <div class="row">
                <div class="col-s-12 col-lg-6">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        <i class="material-icons">person_add</i> Agregar Empleado
                    </button>
                </div>

                <div class="col-s-12 col-lg-6">
                    <select class="custom-select">
                        <option selected>Filtrar Empleados</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>
                </div>
            </div>
            

        
            <br />
            <br />
            <div class="table-responsive">
            <table class="table table-striped">
                <caption>Listado de Empleados</caption>
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">Cedula</th>
                        <th scope="col">Email</th>
                        <th scope="col">Fecha Nacimiento</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>123</td>
                        <td>@mdo</td>
                        <td>12/12/12</td>
                        <td>
                            <a href="perfil_empleado.aspx">
                                <i class="material-icons acciones" data-toggle="modal" data-target="#datosModal" data-toggle="tooltip" data-placement="top" title="Mostrar Empleado">assignment_ind</i>
                            </a>
                           <i class="material-icons acciones" data-toggle="tooltip" data-placement="top" title="Modificar Empleado">update</i>
                           <i class="material-icons  acciones text-success" data-toggle="tooltip" data-placement="top" title="Estado">check_circle</i>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                    </tr>
                </tbody>
            </table>
        </div>
       </div> <!--fin table responsive-->
        

         <!-- Modal-->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar Datos del Empleado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
        
          <div class="form-group row">
            <asp:Label runat="server" AssociatedControlID="nombre" CssClass="col-md-2 control-label" Text="Nombre"></asp:Label>
            <div class="col-sm-10">
                <asp:TextBox runat="server" ID="nombre" CssClass="form-control" />
            </div>
          </div>

          <div class="form-group row">
            <asp:Label runat="server" AssociatedControlID="apellido" CssClass="col-md-2 control-label" Text="Apellido"></asp:Label>
            <div class="col-sm-10">
                <asp:TextBox runat="server" ID="apellido" CssClass="form-control" />
            </div>
          </div>

           <div class="form-group row">
               <asp:Label AssociatedControlID="cedula" Text="Cedula" runat="server" CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-sm-10">
              <asp:TextBox runat="server" ID="cedula" CssClass="form-control" placeholder="12345678"/>
            </div>
          </div>

           <div class="form-group row">
            <asp:Label runat="server" AssociatedControlID="email" Text="Email" CssClass="col-md-2 control-label"></asp:Label>
            <div class="col-sm-10">
              <asp:TextBox runat="server" ID="email" CssClass="form-control" required placeholder="alguien@email.com"/>
            </div>
          </div>

           <div class="form-group row">
               <asp:Label runat="server" ID="Label1" AssociatedControlID="foto" Text="Foto" CssClass="col-md-2 control-label"></asp:Label>
               <div class="col-sm-10">
                   <asp:FileUpload ID="foto" CssClass="form-control" runat="server" />
               </div>
           </div>

  
          <div class="form-group row">
            <div class="col-sm-10">
              <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
          </div>

              </div>
            </div>
          </div>
        </div>
        <!-- fin modal-->
    </form>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

</body>
</html>
